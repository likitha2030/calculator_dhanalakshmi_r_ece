def cal(query):
    q1=query
    if (q1[0].isdigit()):
        arr=list();
        num=''
        for i in q1:
            if i.isdigit():
                num=num+i
            else:
                arr.append(num);
                arr.append(i)
                num=''
        arr.append(i)
        op=arr[1];
        if op== '+':
            res=int(arr[0])+int(arr[2])
        elif op=='-':
            res=int(arr[0])-int(arr[2])
        elif op=='*':
            res=int(arr[0])*int(arr[2])
        elif op=='/':
            res=int(arr[0])/int(arr[2])
        print("output=",res)
    elif 'area'in q1:
        arr=list();
        num=''
        for i in q1[5:]:
            if i.isdigit():
                num=num+i
            elif num!='':
                arr.append(num);
                num=''
        if 'triangle' in q1:
            res=(int(arr[0])*int(arr[1]))/2
        elif 'parallelogram' in q1:
            res=int(arr[0])*int(arr[1])
        elif 'circle' in q1:
            res=3.1415*(int(arr[0])^2)
        print("output",res,'cm^2')
    elif 'volume' in q1:
        arr=list();
        num=''
        for i in q1[5:]:
            if i.isdigit():
                num=num+i
            elif num!='':
                arr.append(num);
                num=''
        if 'cone' in q1:
            res=(3.14159265/3)*int(arr[0])*int(arr[0])*int(arr[1])
        elif 'rectangular' in q1:
            if 'ft' in q1:
                res=int(arr[0])*int(arr[1])*int(arr[2])*30.48
            else:
                res=int(arr[0])*int(arr[1])*int(arr[1])                
        elif 'sphere' in q1:
            res=3.1415*(4/3)*(int(arr[0])^3)
        print("output",res,'cm^3')

cal("12-3")
cal("area(parallelogram(12cm,13cm)")
cal("volume(rectangular-tank,12cm,3cm,3ft))")
cal("volume(cone,3cm,2cm)")

